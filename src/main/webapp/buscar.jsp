<%@page import="javax.json.JsonObject"%>
<%@page import="javax.json.JsonReader"%>
<%@page import="javax.json.JsonReader"%>
<%@page import="javax.json.JsonArray"%>
<%@page import="javax.json.Json"%>
<%@page import="java.io.StringReader"%>
<%@page import="javax.ws.rs.client.ClientBuilder"%>
<%@page import="javax.ws.rs.core.Response"%>
<%@page import="javax.ws.rs.client.Client"%>
<%@page import="javax.ws.rs.client.Client"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <%
        String url = "http://pelisrestfree.herokuapp.com/api/pelis";
        Client cliente = ClientBuilder.newClient();
        Response r = cliente.target(url).request().get();

        String respuesta = r.readEntity(String.class);
        JsonReader j = Json.createReader(new StringReader(respuesta));
        JsonArray pelisA = j.readArray();
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" >
    </head>
    <body>
        <nav>
            <ul>
                <li><a href="index.jsp">Tabla</a></li>
                <li><a href="crear.jsp">Crear</a></li>
                <li><a href="buscar.jsp">Buscar</a></li>
                <li><a href="acerca.jsp">Acerca de</a></li>
            </ul>
        </nav>
        <div class="container">
            <div class="row justify-content-md-center">
                <select onchange="buscar(value)">
                    <option value="0" selected>Seleccione una pelicula</option>
                    <% for (int i = 0; i < pelisA.size(); i++) {
                            JsonObject pelicula = pelisA.getJsonObject(i);
                    %>
                    <option value="<%= pelicula.getInt("id")%>" name="<%= pelicula.getInt("id")%>"><%= pelicula.getString("peli_name")%></option>

                    <%
                        }
                    %>
                </select>
            </div>
            <div class="row justify-content-md-center">
                <table id="tabla" class="table table-striped table-bordered" style="display: none;" >
                    <thead>
                        <th>id</th>
                        <th>Nombre</th>
                        <th>Reseña</th>
                        <th>Categoria</th>
                        <th>Reparto</th>
                        <th>Director</th>
                        <th>Año</th>
                    </thead>
                    <tbody>
                        <td><span id="id"></span></td>
                        <td><span id="peli_name"></span></td>
                        <td><span id="peli_review"></span></td>
                        <td><span id="peli_category"></span></td>
                        <td><span id="peli_cast"></span></td>
                        <td><span id="peli_director"></span></td>
                        <td><span id="peli_year"></span></td>
                        <td><a id="eliminar">Eliminar</a></td>
                        <td><a onclick="editar()">Editar</a></td>
                    </tbody> 
                </table>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-md-center">
                <form id="editarpeliculas" style="display: none">
                    <label for="ids" > Id</label>
                    <input id="ids" type="number" name="id" required />
                    <label for="peli_names" > Nombre</label>
                    <input id="peli_names" type="text" name="peli_name" required />
                    <label for="peli_reviews" > Reseña</label>
                    <input id="peli_reviews" type="text" name="peli_review" required />
                    <label for="peli_categorys" > Categoria</label>
                    <input id="peli_categorys" type="text" name="peli_category" required />
                    <label for="peli_casts" > Reparto</label>
                    <input id="peli_casts" type="text" name="peli_cast" required />
                    <label for="peli_directors" > Director</label>
                    <input id="peli_directors" type="text" name="peli_director" required />
                    <label for="peli_years" > Año</label>
                    <input id="peli_years" type="number" name="peli_year" required />
                    <input type="submit">
                </form>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>  
        <script>

                            function buscar(id) {
                                /*var settings = {
                                    "async": true,
                                    "crossDomain": true,
                                    "url": "https://pelisrestfree.herokuapp.com/api/pelis/" + id,
                                    "method": "GET",
                                    "dataType": "json",
                                    "headers": {
                                        "contentType": "application/json; charset=utf-8", // this
                                        "Accept": "/",
                                        "Cache-Control": "no-cache",
                                        "cache-control": "no-cache",
                                        "async": true,
                                        "crossDomain": true,
                                        "Access-Control-Allow-Origin": "https://pelisrestvista.herokuapp.com/", 
                                        "Access-Control-Allow-Credentials": true,
                                        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,OPTIONS'
                                    }
                                }*/
                                $.ajax({
                                    type: 'GET',
                                    url: "https://pelisrestfree.herokuapp.com/api/pelis/" + id,
                                    contentType: 'application/json',
                                    headers: {"Access-Control-Allow-Origin": "https://pelisrestvista.herokuapp.com/", "Access-Control-Allow-Credentials": "true"},
                                    crossDomain: true,
                                    success: function (data, status, xhr) {
                                        $("#tabla").css("display", "block");
                                        $("#id").append(data.id);
                                        $("#ids").val(data.id);
                                        $("#peli_name").append(data.peli_name);
                                        $("#peli_names").val(data.peli_name);
                                        $("#peli_review").append(data.peli_review);
                                        $("#peli_reviews").val(data.peli_review);
                                        $("#peli_category").append(data.peli_category);
                                        $("#peli_categorys").val(data.peli_category);
                                        $("#peli_cast").append(data.peli_cast);
                                        $("#peli_casts").val(data.peli_cast);
                                        $("#peli_director").append(data.peli_director);
                                        $("#peli_directors").val(data.peli_director);
                                        $("#peli_year").append(data.peli_year);
                                        $("#peli_years").val(data.peli_year);
                                        $('#eliminar').attr('onclick', 'eliminar(' + data.id + ')');
                                    },
                                    error: function (xhr, status, error) {
                                        alert(error);
                                    }
                                    
                                });
                            }

                            function eliminar(id) {
                                var eliminar = {
                                    "async": true,
                                    "crossDomain": true,
                                    "url": "https://pelisrestfree.herokuapp.com/api/pelis/" + id,
                                    "method": "DELETE",
                                    headers: {"Access-Control-Allow-Origin": "http://localhost:8080", "Access-Control-Allow-Credentials": "true"}
                                }
                                $.ajax(eliminar).done(function (response) {
                                    location.reload();
                                });
                            }

                            function editar() {
                                $("#editarpeliculas").css("display", "block");
                            }

                            $("#editarpeliculas").submit(function (event) {
                                event.preventDefault();
                                var form = $(this);
                                var id = form.find('input[name="id"]').val();
                                var peli_name = form.find('input[name="peli_name"]').val();
                                var peli_review = form.find('input[name="peli_review"]').val().replace(",", "");
                                var peli_category = form.find('input[name="peli_category"]').val();
                                var peli_cast = form.find('input[name="peli_cast"]').val();
                                var peli_director = form.find('input[name="peli_director"]').val();
                                var peli_year = form.find('input[name="peli_year"]').val();
                                var url = 'https://pelisrestfree.herokuapp.com/api/pelis';
                                var jsonString = JSON.stringify({id: id, peli_cast: peli_cast, peli_category: peli_category, peli_director: peli_director, peli_name: peli_name, peli_review: peli_review, peli_year: peli_year});
                                console.log(jsonString);
                                $.ajax({
                                    type: 'PUT',
                                    url: url,
                                    contentType: 'application/json',
                                    headers: {"Access-Control-Allow-Origin": "https://pelisrestvista.herokuapp.com/", "Access-Control-Allow-Credentials": "true"},
                                    crossDomain: true,
                                    data: jsonString,
                                    success: function (data, status, xhr) {
                                        location.reload();
                                    },
                                    error: function (xhr, status, error) {
                                        alert(error);
                                    }
                                });
                            });
        </script>
    </body>
</html>
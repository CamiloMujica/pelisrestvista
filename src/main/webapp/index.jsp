<%@page import="javax.json.JsonObject"%>
<%@page import="javax.json.JsonArray"%>
<%@page import="javax.json.JsonReader"%>
<%@page import="javax.json.Json"%>
<%@page import="java.io.StringReader"%>
<%@page import="javax.ws.rs.core.Response"%>
<%@page import="javax.ws.rs.client.ClientBuilder"%>
<%@page import="javax.ws.rs.client.Client"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        String url = "https://pelisrestfree.herokuapp.com/api/pelis";
        Client client = ClientBuilder.newClient();
        Response r = client.target(url).request().get();

        String respuesta = r.readEntity(String.class);
        JsonReader j = Json.createReader(new StringReader(respuesta));
        JsonArray arr = j.readArray();
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pelis CRUD</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" crossorigin="anonymous">
    </head>
    <body>
        <nav>
            <ul>
                <li><a href="index.jsp">Tabla</a></li>
                <li><a href="crear.jsp">Crear</a></li>
                <li><a href="buscar.jsp">Buscar</a></li>
                <li><a href="acerca.jsp">Acerca de</a></li>
            </ul>
        </nav>
        <div class="container">
            <div class="row justify-content-md-center">
                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Resumen</th>
                            <th scope="col">Reparto</th>
                            <th scope="col">Categoria</th>
                            <th scope="col">Director</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% for (int i = 0; i < arr.size(); i++) {
                                JsonObject pelis = arr.getJsonObject(i);
                        %>
                        <tr>
                            <th><%= pelis.getInt("id")%></th>
                            <td><%= pelis.getString("peli_name")%></td>
                            <td><%= pelis.getInt("peli_year")%></td>
                            <td><%= pelis.getString("peli_review")%></td>
                            <td><%= pelis.getString("peli_cast")%></td>
                            <td><%= pelis.getString("peli_category")%></td>
                            <td><%= pelis.getString("peli_director")%></td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>                
            </div>
        </div>
    </body>
</html>
